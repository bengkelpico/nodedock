<!---
 Copyright (c) 2017 Julian Prasetyo <picobug.jp@gmail.com>
 
 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

# Feel Free

## Can I Use ?

Yes, this repo is free with MIT License.

## How this work ?

This repo use docker to run well, so you learn getting started docker to use it. And this repo use docker-compose to manage docker container, so learn it too.

## What best practice to run this ?

Ok, you can follow my instruction to run it well. But I don't teach how to install docker and docker-compose for your machine. Follow from official site to install it Dude. :) Let's play fun and don't forget some cup of coffe too.

1. Open your terminal/command promp if you use windows.
2. Go to path this repo you place it
3. Type "docker-compose up -d"
4. Wait until finish, and foila goto your browser
5. Type "localhost" and you can look "Hello World!"
6. Congrats this repo work like a charm (Y)